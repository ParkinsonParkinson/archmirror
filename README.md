# Arch Mirror
A _too_-basic couple of scripts to create a mirror of an Arch repository with rsync.
It allows you to only retreive files newer than when you last updated it, so you can download fewer data.
I do not intend to put much more effort in it, but I made it because I couldn't find a better one, so I figured I should share.

## Usage
Update the last updated time in filter_files.py and any paths you want changed.
Then just run `mirror.sh`.
The script will:
1. Create a raw list of all files in the repository,
2. Create a new file with a list of all files newer than the last_update variable, and
3. Download all these files.