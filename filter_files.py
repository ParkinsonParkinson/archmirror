#!/usr/bin/python3

import sys
import datetime

last_update = 1556780706  # Calculate manually or take from lastupdate

def main():
        # Yeah, this should probably be taken from args
	with open('generated/raw_file_list.txt') as file_list_file:
		file_list = file_list_file.readlines()

	for file in file_list[2:-4]:  # First two lines and last few lines aren't files, probably could be changed with rsync flags
		try:
			mod_time, file_path = file.strip().split(' ')
			unix_time = datetime.datetime.strptime(mod_time, "%Y/%m/%d-%H:%M:%S").timestamp()
		except Exception as e:
			print(f"File '{file}' failed: {e}", file=sys.stderr)
		else:
			if unix_time > last_update:
				print(file_path)
			
if __name__ == "__main__":
	main()
