#!/bin/bash
# Could probably detect the optimal source automatically
src=rsync://mirror.23media.com/archlinux/
dst=actual_mirror

# archive - preserve symlinks, permissions, times, group, owner, devices and special files, and recurse
RSYNC_FLAGS="--archive --verbose --hard-links --progress --safe-links --no-motd --exclude-from=exclude_list.txt"

echo "Creating raw file list"
# rsync dry run to create file list
rsync --dry-run $RSYNC_FLAGS --out-format='%M %f' "$src" "$dst" > generated/raw_file_list.txt

echo "Make sure you updated the time in filter_files.py!"
echo "Filtering files by date"
# It's possible do either do it all in Python or in Bash
# But neither one is exactly right for the job
python3 filter_files.py > generated/final_file_list.txt

echo "Downloading files to $dst!"
# This could probably be improved with `parallel` in two ways:
# 1. Divide the final_file_list.txt into chunks and let each rsync process take one
# 2. Retreive the files from different arch mirrors, each rsync process taking one
rsync $RSYNC_FLAGS --delay-updates --files-from=generated/final_file_list.txt "$src" "$dst"

